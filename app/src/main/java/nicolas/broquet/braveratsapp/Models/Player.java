package nicolas.broquet.braveratsapp.Models;

import java.util.ArrayList;
import java.util.Collections;

public class Player {
    protected String name;
    protected Integer type;
    protected Integer score = 0;
    protected ArrayList<Cards> hand = new ArrayList<Cards>();

    public Player() {
        // Create empty player object
    }

    public Player(Integer type) {
        this.type = type;
    }

    public Player(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public ArrayList<Cards> getHand() {
        return hand;
    }

    public void setHand(ArrayList<Cards> hand) {
        this.hand = hand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void shuffleHand() {
        Collections.shuffle(this.hand);
    }
}
