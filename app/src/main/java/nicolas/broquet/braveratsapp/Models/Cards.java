package nicolas.broquet.braveratsapp.Models;

import android.media.Image;

public class Cards {
    private String id;
    private String nom;
    private String description;
    private long valeur;
    private String color;
    private Image image;


    public Cards() {
        // Empty cards object
    }

    public Cards(String id, long valeur, String color) {
        this.id = id;
        this.valeur = valeur;
        this.color = color;
    }

    public Cards(String id, String nom, long valeur, String color) {
        this.id = id;
        this.nom = nom;
        this.valeur = valeur;
        this.color = color;
    }

    public Cards(String id, String nom, String description, long valeur, String color) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.valeur = valeur;
        this.color = color;
    }

    public Cards(String id, String nom, String description, long valeur, String color, Image image) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.valeur = valeur;
        this.color = color;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getValeur() {
        return valeur;
    }

    public void setValeur(long valeur) {
        this.valeur = valeur;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
