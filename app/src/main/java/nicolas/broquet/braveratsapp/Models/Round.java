package nicolas.broquet.braveratsapp.Models;

public class Round {
    String idRound;
    String p1move = "";
    String p2move = "";
    String roundWinner = "";

    public Round() {
    }

    // Création d'un round par l'hôte
    public Round(String idRound) {
        this.idRound = idRound;
        this.p1move = "";
        this.p2move = "";
        this.roundWinner = "";
    }

    public String getIdRound() {
        return idRound;
    }

    public void setIdRound(String idRound) {
        this.idRound = idRound;
    }

    public String getP1move() {
        return p1move;
    }

    public void setP1move(String p1move) {
        this.p1move = p1move;
    }

    public String getP2move() {
        return p2move;
    }

    public void setP2move(String p2move) {
        this.p2move = p2move;
    }

    public String getRoundWinner() {
        return roundWinner;
    }

    public void setRoundWinner(String roundWinner) {
        this.roundWinner = roundWinner;
    }
}
