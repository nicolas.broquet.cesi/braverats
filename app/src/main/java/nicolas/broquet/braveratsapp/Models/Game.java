package nicolas.broquet.braveratsapp.Models;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Game {
    private String roomName;
    private Integer playerTurn;
    private Integer winPool;
    private Boolean gameover = false;
    private String winner;

    public Game() {
        // Create Room empty object
    }

    public Game(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Integer getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(Integer playerTurn) {
        this.playerTurn = playerTurn;
    }

    public Integer getWinPool() {
        return winPool;
    }

    public void setWinPool(Integer winPool) {
        this.winPool = winPool;
    }

    public Boolean getGameover() {
        return gameover;
    }

    public void setGameover(Boolean gameover) {
        this.gameover = gameover;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
