package nicolas.broquet.braveratsapp.EventManager;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;

import nicolas.broquet.braveratsapp.Models.Round;

public class HostManager extends GameManager {
    private static final String TAG = "HostManager";

    @Override
    public void updateWinPool(DocumentReference reference, int winPool) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        reference.update("winPool", winPool);
    }

    @Override
    public void resetWinPool(DocumentReference reference) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        reference.update("winPool", 0);
    }

    @Override
    public void updateScore(DocumentReference hostRef, DocumentReference guestRef, int p1Score, int p2Score) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        hostRef.update("score", p1Score);
        guestRef.update("score", p2Score);
    }

    @Override
    public void addNewRound(CollectionReference reference, Round currentRound) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName() + " : " + (new Integer(currentRound.getIdRound()) + 1));

        int newId = Integer.parseInt(currentRound.getIdRound()) + 1;
        reference.document(Integer.toString(newId)).set(new Round(Integer.toString(newId)));
    }

    @Override
    public void closeGameRoom(CollectionReference reference, String roomName) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        reference.document(roomName).update("closed", true);
    }
}
