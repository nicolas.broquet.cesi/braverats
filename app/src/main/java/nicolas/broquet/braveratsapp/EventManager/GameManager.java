package nicolas.broquet.braveratsapp.EventManager;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;

import nicolas.broquet.braveratsapp.Models.Cards;
import nicolas.broquet.braveratsapp.Models.Round;

public class GameManager {
    private static final String TAG = "GameManager";

    public GameManager() {

    }

    public void updateHand(DocumentReference reference, ArrayList<Cards> hand) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        reference.update("hand", hand);
    }

    public void updateRoundMove(CollectionReference roundsColRef, String roundID, Round currentRound) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        roundsColRef.document(roundID).set(currentRound, SetOptions.merge());
    }

    public void updatePlayerTurn(DocumentReference reference, HashMap<String, Object> game_PlayerTurn) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
        reference.set(game_PlayerTurn, SetOptions.merge());
    }

    public void updateWinPool(DocumentReference reference, int winPool) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
    }

    public void resetWinPool(DocumentReference reference) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
    }

    public void updateScore(DocumentReference hostRef, DocumentReference guestRef, int p1Score, int p2Score) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
    }

    public void addNewRound(CollectionReference reference, Round currentRound) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName() + " : " + (new Integer(currentRound.getIdRound()) + 1));
    }

    public void closeGameRoom(CollectionReference reference, String roomName) {
        Log.i(TAG, new Object() {}.getClass().getEnclosingMethod().getName());
    }
}
