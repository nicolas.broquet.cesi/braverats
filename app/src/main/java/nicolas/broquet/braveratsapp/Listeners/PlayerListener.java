package nicolas.broquet.braveratsapp.Listeners;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import nicolas.broquet.braveratsapp.Activities.GameActivity;
import nicolas.broquet.braveratsapp.Models.Player;
import nicolas.broquet.braveratsapp.R;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.ShowToast;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.UpdatePlayersObjects;

public class PlayerListener {
    private static final String TAG = "PlayerListener";

    // Context parent
    private Activity parentActivity;
    // Reference listener
    private CollectionReference playersColRef;
    // Interfaces et Méthodes
    private ShowToast showToast;
    private UpdatePlayersObjects updatePlayersList;
    // Listener + Registration
    private ListenerRegistration playerListener;
    // Player info
    private Integer playerType;

    public PlayerListener(GameActivity parent, CollectionReference playersColRef, Integer playerType) {
        this.parentActivity = parent;
        this.playerType = playerType;
        this.playersColRef = playersColRef;


        // Références méthodes des interfaces
        this.showToast = parent::onShowToast;
        this.updatePlayersList = parent::onUpdatePlayersList;

        addListener();
    }

    private void addListener() {
        playerListener = playersColRef.addSnapshotListener(parentActivity, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot querySnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    showToast.onShowToast(R.string.error_loading_game);
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (querySnapshot != null) {
                    // On récupère les joueurs
                    List<Player> players = new ArrayList<>();

                    // Conversion en Player
                    for (QueryDocumentSnapshot doc : querySnapshot) {
                        players.add(doc.toObject(Player.class));
                    }

                    updatePlayersList.onUpdatePlayersList(players);
                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });
    }

    public void removeListener() {
        playerListener.remove();
    }
}
