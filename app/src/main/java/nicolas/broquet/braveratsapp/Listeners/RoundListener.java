package nicolas.broquet.braveratsapp.Listeners;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import nicolas.broquet.braveratsapp.Activities.GameActivity;
import nicolas.broquet.braveratsapp.Models.Round;
import nicolas.broquet.braveratsapp.R;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.ShowToast;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.UpdateRoundsObjects;

public class RoundListener {
    private static final String TAG = "RoundListener";

    // Context parent
    private Activity parentActivity;
    // Reference listener
    private CollectionReference roundsColRef;
    // Interfaces et Méthodes
    private ShowToast showToast;
    private UpdateRoundsObjects updateRoundsList;
    // Listener + Registration
    private ListenerRegistration roundListener;

    public RoundListener(GameActivity parent, CollectionReference roundsColRef) {
        this.parentActivity = parent;
        this.roundsColRef = roundsColRef;


        // Références méthodes des interfaces
        this.showToast = parent::onShowToast;
        this.updateRoundsList = parent::onUpdateRoundsList;

        addListener();
    }


    private void addListener() {
        roundListener = roundsColRef.addSnapshotListener(parentActivity, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot querySnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    showToast.onShowToast(R.string.error_loading_game);
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (querySnapshot != null) {
                    // On récupère les joueurs
                    ArrayList<Round> rounds = new ArrayList<>();

                    // Conversion en Player
                    for (QueryDocumentSnapshot doc : querySnapshot) {
                        rounds.add(doc.toObject(Round.class));
                    }

                    updateRoundsList.onUpdateRoundsList(rounds);
                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });
    }

    public void removeListener() {
        roundListener.remove();
    }
}
