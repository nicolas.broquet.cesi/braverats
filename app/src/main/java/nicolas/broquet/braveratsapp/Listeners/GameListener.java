package nicolas.broquet.braveratsapp.Listeners;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import nicolas.broquet.braveratsapp.Activities.GameActivity;
import nicolas.broquet.braveratsapp.Models.Game;
import nicolas.broquet.braveratsapp.R;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.ShowToast;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.UpdateGameObject;

public class GameListener {
    private static final String TAG = "GameListener";

    // Context parent
    private Activity parentActivity;
    // Reference listener
    private DocumentReference gameDocRef;
    // Interfaces et Méthodes
    private ShowToast showToast;
    private UpdateGameObject updateGameObject;
    // Listener + Registration
    private ListenerRegistration gameListener;


    public GameListener(GameActivity parent, DocumentReference gameDocRef) {
        this.parentActivity = parent;
        this.gameDocRef = gameDocRef;


        // Références méthodes des interfaces
        this.showToast = parent::onShowToast;
        this.updateGameObject = parent::onUpdateGame;

        addListener();
    }

    private void addListener() {
        gameListener = gameDocRef.addSnapshotListener(parentActivity, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    showToast.onShowToast(R.string.error_loading_game);
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d(TAG, "Current game data: " + snapshot.getData());
                    updateGameObject.onUpdateGame(snapshot.toObject(Game.class));
                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });
    }

    public void removeListener() {
        gameListener.remove();
    }
}
