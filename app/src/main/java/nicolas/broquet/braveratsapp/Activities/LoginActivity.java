package nicolas.broquet.braveratsapp.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import nicolas.broquet.braveratsapp.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    // View components
    EditText playerNameET;
    Button loginBtn;

    // Variables
    String playerName = "";
    private static final String TAG = "LoginActivity";

    // Database
    FirebaseFirestore db;
    CollectionReference playersRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        playerNameET = findViewById(R.id.playerName_ET);
        loginBtn = findViewById(R.id.login_Btn);

        // Access a Cloud Firestore instance from your Activity
        db = FirebaseFirestore.getInstance();
        // Create a reference to players collection
        playersRef = db.collection("players");

        // Check if the user already exists in preferences
        SharedPreferences preferences = getSharedPreferences("UserPrefs", 0);
        playerName = preferences.getString("playerName", "");
        // Si oui, on est redirigé vers la vue de jeu
        if (!playerName.equals("")) {
            this.startRoomView();
        }
    }

    public void startRoomView(){
        // On démarre la vue des rooms en lui passant le playerName
        Intent intent = new Intent(LoginActivity.this, RoomActivity.class);
        startActivity(intent);
        finish();
    }

    // Si on click sur le bouton de login (pas de user)
    public void createPlayerListener(View v) {
        Log.d(TAG, "Create Player Listener");

        // On récupère le username
        playerName = playerNameET.getText().toString();

        Log.d(TAG, "playerName : " + playerName);

        // Si on a un nom saisi, on l'ajoute dans les SharedPreferences
        if (!playerName.equals("")) {
            // Vérifier en BDD si user existe
            // Create a query against the collection.
            playersRef.whereEqualTo("playerName", playerName).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    Log.d(TAG, "onComplete : ");

                    if (task.isSuccessful()) {
                        String idPlayer = "";

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            idPlayer = document.getId();
                            Log.d(TAG, document.getId() + " => " + document.getData());
                        }

                        // Si on a un player (dernier rentré)
                        if(!idPlayer.equals("")) {
                            // On ajoute le nouvel utilisateur dans les SharedPrefences
                            SharedPreferences preferences = getSharedPreferences("UserPrefs", 0);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("playerName", playerName);
                            editor.putString("playerDoc", idPlayer);
                            editor.apply();

                            // Redirection vers la vue de jeu
                            startRoomView();
                        } else {
                            // Créer utilisateur et redirection
                            createUser();
                        }
                    } else {
                        Log.d(TAG, "Error getting players: ", task.getException());
                    }
                }
            });
        }
    }

    public void createUser() {

        // Create a new player with a first and last name
        Map<String, Object> player = new HashMap<>();
        // Données du document
        player.put("playerName", playerName);

        // Add a new document with a generated ID
        db.collection("players")
                .add(player)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());

                        // On ajoute le nouvel utilisateur dans les SharedPrefences
                        SharedPreferences preferences = getSharedPreferences("UserPrefs", 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("playerName", playerName);
                        editor.putString("playerDoc", documentReference.getId());
                        editor.apply();

                        // Redirection vers la vue de jeu
                        startRoomView();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });

    }
}
