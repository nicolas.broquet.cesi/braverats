package nicolas.broquet.braveratsapp.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import nicolas.broquet.braveratsapp.EventManager.GameManager;
import nicolas.broquet.braveratsapp.EventManager.GuestManager;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.ShowToast;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.UpdateGameObject;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.UpdatePlayersObjects;
import nicolas.broquet.braveratsapp.InterfaceUpdaters.UpdateRoundsObjects;
import nicolas.broquet.braveratsapp.Listeners.GameListener;
import nicolas.broquet.braveratsapp.Listeners.PlayerListener;
import nicolas.broquet.braveratsapp.Listeners.RoundListener;
import nicolas.broquet.braveratsapp.Models.Cards;
import nicolas.broquet.braveratsapp.Models.Game;
import nicolas.broquet.braveratsapp.EventManager.HostManager;
import nicolas.broquet.braveratsapp.Models.Player;
import nicolas.broquet.braveratsapp.Models.Round;
import nicolas.broquet.braveratsapp.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameActivity extends AppCompatActivity implements ShowToast, UpdateGameObject, UpdatePlayersObjects, UpdateRoundsObjects {
    private static final String TAG = "GameActivity";

    // Models
    private Game game;
    private Player player1;
    private Player player2;
    private List<Player> players;
    private List<Round> rounds;
    private Round currentRound;

    private String roomName = "";
    private Integer playerType;

    // Array with all cases [Red 0 - 7][Blue 0 - 7]
    private String[][] winConditions = new String[8][8];
    private String gameWinnerMessage = "";

    // Score des joueurs
    private Integer player1Score = 0;
    private Integer player2Score = 0;

    // Cards
    private HashMap<String, Integer> assetsHashmap = new HashMap<String, Integer>();

    // Database
    private FirebaseFirestore db;
    private CollectionReference roomsColRef;
    private CollectionReference gameColRef;
    private CollectionReference playersColRef;
    private CollectionReference roundsColRef;
    private DocumentReference gameDocRef;
    private DocumentReference hostDocRef;
    private DocumentReference guestDocRef;

    // Listener
    private GameListener gameListener;
    private PlayerListener playerListener;
    private RoundListener roundListener;

    // UI
    private LinearLayout zoneJoueur;
    private LinearLayout zoneEnnemi;
    private TextView nomJoueur;
    private TextView nomEnnemi;
    private TextView scoreJoueur;
    private TextView scoreJoueur2;
    private ImageView playerPlaceholder;
    private ImageView opponentPlaceholder;
    private TextView gameoverTV;

    // Others
    protected GameManager gameManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // get Intent
        Intent intent = getIntent();
        playerType = intent.getIntExtra("playerType", 0);
        roomName = intent.getStringExtra("roomName");

        // UI
        // Zone des cartes des joueurs
        zoneJoueur = findViewById(R.id.playerHand_Linear);
        zoneEnnemi = findViewById(R.id.opponentHand_Linear);
        // Placeholder des images jouées
        playerPlaceholder = findViewById(R.id.player_placeholder);
        opponentPlaceholder = findViewById(R.id.opponent_placeholder);
        // Nom joueurs
        nomJoueur = findViewById(R.id.playerName_TV);
        nomEnnemi = findViewById(R.id.opponentName_TV);
        // Score joueurs
        scoreJoueur = findViewById(R.id.playerScore_TV);
        scoreJoueur2 = findViewById(R.id.opponentScore_TV);

        // Database + ref collections & documents
        db = FirebaseFirestore.getInstance();
        roomsColRef = db.collection("rooms");
        gameColRef = roomsColRef.document(roomName).collection("game");
        playersColRef = roomsColRef.document(roomName).collection("players");
        roundsColRef = roomsColRef.document(roomName).collection("rounds");

        gameDocRef = gameColRef.document("game");
        hostDocRef = playersColRef.document("player1");
        guestDocRef = playersColRef.document("player2");

        // Vérifie si on est l'hôté
        if (playerType.equals(1)) {
            this.gameManager = new HostManager();
        } else {
            this.gameManager = new GuestManager();
        }

        // Tableau des conditions de victoire
        this.fillWinConditionsArray();

        // On récupère les assets
        this.setAssetsHashmap();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        // On s'abonne à la partie
        gameListener = new GameListener(this, gameDocRef);
        // On s'abonne aux rounds
        roundListener = new RoundListener(this, roundsColRef);
        // On s'abonne à la collection des joueurs
        playerListener = new PlayerListener(this, playersColRef, playerType);
    }

    @Override
    public void onStop() {
        super.onStop();

        // On supprime tous les abonnements
        gameListener.removeListener();
        playerListener.removeListener();
        roundListener.removeListener();
    }

    // Interfaces
    @Override
    public void onShowToast(Integer message) {
        Toast.makeText(GameActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUpdateGame(Game game) {
        this.game = game;
    }

    @Override
    public void onUpdatePlayersList(List<Player> players) {
        this.players = players;
        this.player1 = players.get(0);
        this.player2 = players.get(1);

        // Mise à jour de l'UI
        updatePlayerUI();

        // Mise à jour des cartes
        updateCardsUI();
    }

    @Override
    public void onUpdateRoundsList(ArrayList<Round> rounds) {
        this.rounds = rounds;

        // On récupère le round courant
        currentRound = getCurrentRound();
        // Gestion des zones de jeu
        updatePlaceholderUI(currentRound);

        // Si les 2 joueurs ont joué, on trouve le gagnant et on ajoute un nouveau round
        if (!currentRound.getP1move().equals("") && !currentRound.getP2move().equals("") && currentRound.getRoundWinner().equals("")) {
            roundEnded(currentRound);
        }
    }

    public void roundEnded(Round currentRound) {
        // Get the round winner
        String roundWinner = getRoundWinner(currentRound.getP1move(), currentRound.getP2move(), "");

        incrementWinPool(1);

        // Affichage du gagnant
        String resultMessage = "";
        switch (roundWinner) {
            case "R":
                resultMessage = getResources().getString(R.string.winner_host, player1.getName());

                if (currentRound.getP1move().equals("R_4")) {
                    incrementWinPool(1);
                }

                break;
            case "B":
                resultMessage = getResources().getString(R.string.winner_guest, player2.getName());

                if (currentRound.getP2move().equals("B_4")) {
                    incrementWinPool(1);
                }

                break;
            case "X":
                resultMessage = getResources().getString(R.string.winner_wait);
                break;
            case "X1":
                resultMessage = getResources().getString(R.string.winner_other, "X1");
                break;
            case "X2":
                resultMessage = getResources().getString(R.string.winner_other, "X2");
                break;
            case "X3":
                resultMessage = getResources().getString(R.string.winner_other, "X3");
                break;
            default:
                resultMessage = getResources().getString(R.string.winner_other, "");
        }


        // On affiche le gagant
        Toast.makeText(GameActivity.this, resultMessage, Toast.LENGTH_LONG).show();

        // MAJ du pool en DB
        endRound(roundWinner);

        // Update rounds & score
        roundsColRef.document(currentRound.getIdRound()).update("roundWinner", roundWinner).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                System.out.println(TAG + "Manager => Affichage des score : (J1)" + player1Score + "|" + "(J2)" + player2Score);

                // Ajout d'un nouveau round vide (1 seule fois)
                gameManager.addNewRound(roundsColRef, currentRound);

                // UI : Om remplace les images par les placeholder
                playerPlaceholder.setImageResource(R.drawable.card_placeholder);
                opponentPlaceholder.setImageResource(R.drawable.card_placeholder);

                // Si les 8 rounds sont finis, on détermine le gagnant et on termine la partie
                if (currentRound.getIdRound().equals("7")) {
                    gameover();
                }
            }
        });
    }

    public void gameover() {
        // Fin winner
        if (player1.getScore() > player2.getScore()) {
            gameWinnerMessage = getResources().getString(R.string.gameWinner, player1.getName());
            game.setWinner("player1");
        } else if (player1.getScore() < player2.getScore()) {
            gameWinnerMessage = getResources().getString(R.string.gameWinner, player2.getName());
            game.setWinner("player2");
        } else if (player1.getScore().equals(player2.getScore())) {
            gameWinnerMessage = getResources().getString(R.string.gameWinner_draw);
            game.setWinner("draw");
        } else {
            gameWinnerMessage = getResources().getString(R.string.gameWinner_other);
            game.setWinner("Not implemented");
        }

        // Fin de partie
        game.setGameover(true);

        // Database : Fin de partie
        gameDocRef.set(game, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(GameActivity.this, gameWinnerMessage, Toast.LENGTH_LONG).show();

                // TODO : Ditach all click listeners
                zoneJoueur.setClickable(false);
                zoneEnnemi.setClickable(false);

                // Affichage gameover
                gameoverTV = findViewById(R.id.gameover_TV);
                gameoverTV.setVisibility(View.VISIBLE);

                // Fermeture de la room
                gameManager.closeGameRoom(roomsColRef, roomName);
            }
        });
    }

    // Listener methods
    public void updatePlayerUI() {
        // Noms
        if (playerType.equals(1)) {
            nomJoueur.setText(player1.getName());
            nomEnnemi.setText(player2.getName());
        } else {
            nomJoueur.setText(player2.getName());
            nomEnnemi.setText(player1.getName());
        }

        // Scores
        // Mettre à jour l'affichage
        if (playerType.equals(1)) {
            scoreJoueur.setText(player1.getScore().toString());
            scoreJoueur2.setText(player2.getScore().toString());
        } else {
            scoreJoueur.setText(player2.getScore().toString());
            scoreJoueur2.setText(player1.getScore().toString());
        }
    }

    public void updateCardsUI() {
        // Mettre à jour l'affichage
        // UI : On supprime les cartes pour les recréer si modification du document
        zoneJoueur.removeAllViews();
        zoneEnnemi.removeAllViews();

        // Traitement pour chaque joueur
        players.forEach((currPlayer) -> {
            // Traitement pour chaque carte
            currPlayer.getHand().forEach((currCard) -> {
                // On crée une image, on lui ajoute la ressource
                ImageView iv = new ImageView(GameActivity.this);

                // Paramètre de l'imageView
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        /*width*/ ViewGroup.LayoutParams.WRAP_CONTENT,
                        /*height*/ ViewGroup.LayoutParams.MATCH_PARENT,
                        /*weight*/ 1.0f
                );
                iv.setLayoutParams(param);

                // On ajoute l'id de la carte en tant que tag à l'imageView
                iv.setTag(currCard.getId());

                // En fonction du type du joueur courant
                if ((playerType.equals(1) && currPlayer.getType().equals(1)) || (playerType.equals(2) && currPlayer.getType().equals(2))) {
                    // On ajoute l'asset recto
                    iv.setImageResource(assetsHashmap.get(currCard.getId()));

                    // Click listener d'une carte
                    iv.setOnClickListener((cardView) -> {
                        // Si c'est le tour du joueur courant
                        if (isItMyTurn(currPlayer)) {
                            // On supprime la carte jouée
                            playCard(currPlayer, currCard);
                            // Maj du Round
                            addMove(cardView);
                            // Au tour du joueur suivant
                            changePlayerTurn();
                        }
                    });

                    // UI : Ajout
                    zoneJoueur.addView(iv);
                } else {
                    // On ajoute l'assert verso à la carte
                    iv.setImageResource(R.drawable.verso);

                    // UI : Ajout
                    iv.setRotation(180);
                    zoneEnnemi.addView(iv);
                }
            });
        });
    }

    private boolean isItMyTurn(Player p) {
        return game.getPlayerTurn().equals(p.getType());
    }

    private void playCard(Player p, Cards c) {
        // UI : Supprime la vue de la carte
        p.getHand().remove(c);
        // Database : On actualise les mains
        if (p.getType().equals(1)) {
            gameManager.updateHand(hostDocRef, p.getHand());
        } else {
            gameManager.updateHand(guestDocRef, p.getHand());
        }
    }

    private void addMove(View cardView) {
        // Mouvement, on ajoute le mouvement du joueur courant dans le round
        if (game.getPlayerTurn().equals(1)) {
            currentRound.setP1move(cardView.getTag().toString());
        } else if (game.getPlayerTurn().equals(2)) {
            currentRound.setP2move(cardView.getTag().toString());
        }
        // Database : Maj du round
        gameManager.updateRoundMove(roundsColRef, currentRound.getIdRound(), currentRound);
    }

    private void changePlayerTurn() {
        // TODO : Exception si voleur
        // Nouveau tour
        HashMap<String, Object> updateGameData = new HashMap<>();
        game.setPlayerTurn((game.getPlayerTurn() == 1) ? 2 : 1);
        updateGameData.put("playerTurn", game.getPlayerTurn());
        // Database : Maj du tour
        gameManager.updatePlayerTurn(gameDocRef, updateGameData);
    }

    private Round getCurrentRound() {
        return rounds.get(rounds.size() - 1);
    }

    private void updatePlaceholderUI(Round currentRound) {
        // Host - Player1
        if (playerType.equals(1)) {
            if (!currentRound.getP1move().equals("")) {
                playerPlaceholder.setImageResource(assetsHashmap.get(currentRound.getP1move()));
            }
            if (!currentRound.getP2move().equals("")) {
                opponentPlaceholder.setImageResource(R.drawable.verso);
            }
        }
        // Guest - Player2
        if (playerType.equals(2)) {
            if (!currentRound.getP2move().equals("")) {
                playerPlaceholder.setImageResource(assetsHashmap.get(currentRound.getP2move()));
            }
            if (!currentRound.getP1move().equals("")) {
                opponentPlaceholder.setImageResource(R.drawable.verso);
            }
        }
    }

    // Assets des cartes, acceessible dans une hash (id carte => id ressource)
    private void setAssetsHashmap() {
        assetsHashmap.put("r_0", R.drawable.r_0);
        assetsHashmap.put("r_1", R.drawable.r_1);
        assetsHashmap.put("r_2", R.drawable.r_2);
        assetsHashmap.put("r_3", R.drawable.r_3);
        assetsHashmap.put("r_4", R.drawable.r_4);
        assetsHashmap.put("r_5", R.drawable.r_5);
        assetsHashmap.put("r_6", R.drawable.r_6);
        assetsHashmap.put("r_7", R.drawable.r_7);
        assetsHashmap.put("b_0", R.drawable.b_0);
        assetsHashmap.put("b_1", R.drawable.b_1);
        assetsHashmap.put("b_2", R.drawable.b_2);
        assetsHashmap.put("b_3", R.drawable.b_3);
        assetsHashmap.put("b_4", R.drawable.b_4);
        assetsHashmap.put("b_5", R.drawable.b_5);
        assetsHashmap.put("b_6", R.drawable.b_6);
        assetsHashmap.put("b_7", R.drawable.b_7);
    }

    // Tableau de conditions de victoire
    private void fillWinConditionsArray() {
        winConditions[0][0] = "X3";
        winConditions[0][1] = "X";
        winConditions[0][2] = "X";
        winConditions[0][3] = "X";
        winConditions[0][4] = "X";
        winConditions[0][5] = "B";
        winConditions[0][6] = "X";
        winConditions[0][7] = "X";

        winConditions[1][0] = "X";
        winConditions[1][1] = "X";
        winConditions[1][2] = "B";
        winConditions[1][3] = "R";
        winConditions[1][4] = "B";
        winConditions[1][5] = "B";
        winConditions[1][6] = "B";
        winConditions[1][7] = "R";

        winConditions[2][0] = "X";
        winConditions[2][1] = "R";
        winConditions[2][2] = "B";
        winConditions[2][3] = "R";
        winConditions[2][4] = "B";
        winConditions[2][5] = "B";
        winConditions[2][6] = "B";
        winConditions[2][7] = "B";

        winConditions[3][0] = "X";
        winConditions[3][1] = "B";
        winConditions[3][2] = "B";
        winConditions[3][3] = "X2";
        winConditions[3][4] = "R";
        winConditions[3][5] = "B";
        winConditions[3][6] = "R";
        winConditions[3][7] = "B";

        winConditions[4][0] = "X";
        winConditions[4][1] = "R";
        winConditions[4][2] = "R";
        winConditions[4][3] = "B";
        winConditions[4][4] = "X";
        winConditions[4][5] = "B";
        winConditions[4][6] = "B";
        winConditions[4][7] = "B";

        winConditions[5][0] = "R";
        winConditions[5][1] = "R";
        winConditions[5][2] = "R";
        winConditions[5][3] = "R";
        winConditions[5][4] = "R";
        winConditions[5][5] = "X";
        winConditions[5][6] = "B";
        winConditions[5][7] = "B";

        winConditions[6][0] = "X";
        winConditions[6][1] = "R";
        winConditions[6][2] = "R";
        winConditions[6][3] = "B";
        winConditions[6][4] = "R";
        winConditions[6][5] = "R";
        winConditions[6][6] = "X";
        winConditions[6][7] = "B";

        winConditions[7][0] = "X";
        winConditions[7][1] = "B";
        winConditions[7][2] = "R";
        winConditions[7][3] = "R";
        winConditions[7][4] = "R";
        winConditions[7][5] = "R";
        winConditions[7][6] = "R";
        winConditions[7][7] = "X1";
    }

    // Trouver qui a gagné le round
    private String getRoundWinner(Object p1Card, Object p2Card, String lastResult) {
        // Get cards
        String delimt = "[_]";
        String[] p1CurrCard = p1Card.toString().split(delimt);
        String[] p2CurrCard = p2Card.toString().split(delimt);

        // On vérifie le résultat dans le tableau
        String resultWinCondition = this.winConditions[Integer.parseInt(p1CurrCard[1])][Integer.parseInt(p2CurrCard[1])];

        return resultWinCondition;
    }

    private void incrementWinPool(int i) {
        game.setWinPool(game.getWinPool() + i);
    }

    private void updateScores(String roundWinner) {
        if (roundWinner.equals("R")) {
            player1Score = player1.getScore() + game.getWinPool();
        } else if (roundWinner.equals("B")) {
            player2Score = player2.getScore() + game.getWinPool();
        }

        gameManager.updateScore(hostDocRef, guestDocRef, player1Score, player2Score);
    }

    private void resetWinPool() {
        game.setWinPool(0);
        // Update pool en base
        gameManager.resetWinPool(gameDocRef);
    }

    private void endRound(String roundWinner) {
        // Update pool en base
        gameManager.updateWinPool(gameDocRef, game.getWinPool());

        if (roundWinner.equals("R") || roundWinner.equals("B")) {
            // Update score UI + DB
            updateScores(roundWinner);

            // Reset pool
            resetWinPool();
        }
    }
}
