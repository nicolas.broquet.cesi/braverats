package nicolas.broquet.braveratsapp.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import nicolas.broquet.braveratsapp.Models.Cards;
import nicolas.broquet.braveratsapp.Models.Game;
import nicolas.broquet.braveratsapp.Models.Player;
import nicolas.broquet.braveratsapp.Models.Room;
import nicolas.broquet.braveratsapp.Models.Round;
import nicolas.broquet.braveratsapp.R;
import nicolas.broquet.braveratsapp.Utils.CardXmlParser;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RoomActivity extends AppCompatActivity {

    private static final String TAG = "RoomActivity";

    // UI
    ListView roomsListView;
    Button createRoomBtn;

    // Liste des rooms
    List<String> roomsList = new ArrayList<>();

    // Room sélectionnée ou créée
    String roomName = "";

    // When creating a room
    // New room
    Room room = new Room();
    // New game
    Game game = new Game();
    // Array with all cards
    List allCards = new ArrayList<>();
    // Players
    String playerName = "";

    // Database
    FirebaseFirestore db;
    CollectionReference roomsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        // TODO : Databinding
        // Get UI
        roomsListView = findViewById(R.id.roomsList_LW);
        createRoomBtn = findViewById(R.id.createRoom_Btn);

        // Access a Cloud Firestore instance from your Activity
        db = FirebaseFirestore.getInstance();
        roomsRef = db.collection("rooms");

        // get the player name and assign his room name to the player name
        SharedPreferences preferences = getSharedPreferences("UserPrefs", 0);
        playerName = preferences.getString("playerName", "");
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Liste des rooms existantes, on ajoute un click listener
        roomsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Join existing room and add yourself as player2 if you are not the 1
                roomName = roomsList.get(position);

                createRoomBtn.setText("Joining room");
                createRoomBtn.setEnabled(false);

                // On redirige l'utilisateur sur la vue de jeu
                roomsRef.document(roomName).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Room room = documentSnapshot.toObject(Room.class);

                        // Si le user est le joueur 1/2 ou qu'il n'y pas de joueur 2, on l'ajoute à la liste
                        if (room.getHost().equals(playerName)) {
                            Log.d(TAG, "Welcome player 1");
                            startGameView(1);
                        } else if (room.getGuest().equals(playerName) || room.getGuest().equals("")) {
                            Log.d(TAG, "Welcome player 2");

                            roomsRef.document(roomName).collection("players").document("player2").update("name", playerName).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    roomsRef.document(roomName).update("guest", playerName).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            startGameView(2);
                                        }
                                    });
                                }
                            });

                        }

                        // On ajoute un adapter avec les rooms et on l'ajoute à l'UI
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(RoomActivity.this, android.R.layout.simple_list_item_1, roomsList);
                        roomsListView.setAdapter(adapter);
                    }
                });

            }
        });

        // show if new room is available
        addRoomsEventListener();
    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    // Si on click sur le bouton de login (pas de user)
    public void createRoomListener(View v) {
        createRoomBtn.setText("Creating room");
        createRoomBtn.setEnabled(false);

        // Room configs
        // Create array with all cards
        try {
            // Parser le fichier xml des cartes pour récupérer toutes les cartes du jeu
            CardXmlParser parser = new CardXmlParser();
            InputStream is = getResources().openRawResource(R.raw.card_list);
            this.allCards = parser.parse(is);

            // TODO : Different roomName
            game.setRoomName(playerName);
            game.setPlayerTurn(1);
            game.setWinPool(0);

            // Création des joueurs
            Player player1 = new Player(1, playerName);
            Player player2 = new Player(2, "");

            // Configs players
            player1.setScore(0);
            player2.setScore(0);

            // On distribue les cartes
            player1.setHand(new ArrayList<Cards>(this.allCards.subList(0, (this.allCards.size() / 2))));
            player2.setHand(new ArrayList<Cards>(this.allCards.subList((this.allCards.size() / 2), this.allCards.size())));
            System.out.println("Initial Nombre de carte joueur 1 :" + player1.getHand().size());
            System.out.println("Initial Nombre de carte joueur 2 :" + player2.getHand().size());

            // On mélange les mains
            player1.shuffleHand();
            player1.shuffleHand();

            // Add references
            roomName = playerName;
            room.setHost(playerName);
            room.setGuest("");
            room.setClosed(false);

            // On ajoute la room
            roomsRef.document(roomName).set(room)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "Room added");

                            // On ajoute les infos de jeu, les rounds et les 2 joueurs
                            roomsRef.document(roomName).collection("game").document("game").set(game).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    roomsRef.document(roomName).collection("rounds").document("0").set(new Round("0")).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            roomsRef.document(roomName).collection("players").document("player1").set(player1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    roomsRef.document(roomName).collection("players").document("player2").set(player2).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            // Redirection vers la vue de jeu
                                                            startGameView(1);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error setting room", e);

                            createRoomBtn.setText("Create room");
                            createRoomBtn.setEnabled(true);
                            Toast.makeText(RoomActivity.this, "Accessing room error", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
    }


    private void addRoomsEventListener() {
        // On écoute les MAJ sur les rooms
        roomsRef
                .whereEqualTo("closed", false)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        // Si changement, on met à jour la liste des rooms
                        roomsList.clear();

                        if (value.size() > 0) {
                            // On récupère toutes les rooms
                            for (QueryDocumentSnapshot doc : value) {
                                // On récupère la room
                                Room room = doc.toObject(Room.class);

                                // Si le user est le joueur 1/2 ou qu'il n'y pas de joueur 2, on l'ajoute à la liste
                                if (room.getHost().equals(playerName) || room.getGuest().equals(playerName) || room.getGuest().equals("")) {
                                    roomsList.add(doc.getId());
                                }

                                // On ajoute un adapter avec les rooms et on l'ajoute à l'UI
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(RoomActivity.this, android.R.layout.simple_list_item_1, roomsList);
                                roomsListView.setAdapter(adapter);
                            }
                        }
                    }
                });
    }

    public void startGameView(Integer playerType) {
        // On démarre la vue de jeu en lui passant le nom et le type de joueur et le nom de la pièce
        Intent intent = new Intent(RoomActivity.this, GameActivity.class);
        intent.putExtra("playerType", playerType);
        intent.putExtra("roomName", roomName);

        startActivity(intent);
        finish();
    }
}
