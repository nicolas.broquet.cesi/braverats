package nicolas.broquet.braveratsapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import nicolas.broquet.braveratsapp.R;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends AppCompatActivity {
    Button playBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // Play button - switch to next activity
        playBtn = findViewById(R.id.play_btn);

        // TODO : Testing => Erase shared preferences
        SharedPreferences.Editor editor = getSharedPreferences("UserPrefs", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }

    public void goLoginView(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
