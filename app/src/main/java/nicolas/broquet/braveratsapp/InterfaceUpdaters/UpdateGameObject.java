package nicolas.broquet.braveratsapp.InterfaceUpdaters;

import nicolas.broquet.braveratsapp.Models.Game;

public interface UpdateGameObject {
    public void onUpdateGame (Game game);
}
