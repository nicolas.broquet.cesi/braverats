package nicolas.broquet.braveratsapp.InterfaceUpdaters;

import java.util.List;

import nicolas.broquet.braveratsapp.Models.Player;

public interface UpdatePlayersObjects {
    public void onUpdatePlayersList(List<Player> players);
}
