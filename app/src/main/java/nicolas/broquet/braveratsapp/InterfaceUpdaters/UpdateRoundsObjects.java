package nicolas.broquet.braveratsapp.InterfaceUpdaters;

import java.util.ArrayList;

import nicolas.broquet.braveratsapp.Models.Round;

public interface UpdateRoundsObjects {
    public void onUpdateRoundsList(ArrayList<Round> rounds);
}
