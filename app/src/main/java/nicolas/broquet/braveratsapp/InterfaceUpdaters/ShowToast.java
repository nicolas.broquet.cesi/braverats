package nicolas.broquet.braveratsapp.InterfaceUpdaters;

public interface ShowToast {
    public void onShowToast (Integer message);
}
